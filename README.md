# About ADMIRAL

This project aims at centralizing disclosed cybersecurity incidents in the maritime sector for research activites and other projects in the maritime cybersecurity sector.

# Why the name ADMIRAL?

The acronym stands for **A**dvanced **D**ataset of **M**aritime cyber **I**ncidents **R**ele**a**sed for **L**itterature.

# Sources and information protection

When possible and relevant, the source of the information is given.
Information is strictly derived from OSINT sources. Nothing privileged here.
Project is released with the <span style="color: white;" style="background-color:black">TLP:CLEAR</span>.

# Why ADMIRAL?

- The acronym itself was chosen because an Admiral is a highly-ranked officer within Navies, with a lot of wisdom, knowledge and experience, and willing to share those with his sailors and officers. Well, we humbly hope this dataset is a good way to share our knowledge to enhance cyber awareness in the maritime sector.
- This file was at first invented and compiled by Olivier JACQ on his own blog on maritime cybersecurity ( https://www.cybermaretique.fr ) and he agreed to reformat it and release through the M-CERT on this repository. We wish to thank him for this.
- As Olivier claims "I've worked for more than 20 years now on maritime cybersecurity and I've always been searching for this kind of data for the awareness sessions I give. As there was no real central information, I started to compile a file and released the information on my website. I'm very happy of the M-CERT creation, and I'm happy to share this data if it can be of help to anyone, for awareness, press or research studies."

Please read the CONTRIBUTING and LICENSE file for more information about the contribution process and the proper sourcing for this data. Using, copying or derivating work from this data without respecting the directives given in the LICENSE file is prohibited.

Please note that a web version of the database is being maintained here: https://www.m-cert.fr/admiral/ .
You can also use
- Our RSS feed: https://www.m-cert.fr/admiral/admiral.xml
- Our JSON feed: https://www.m-cert.fr/admiral/admiral.json
- Our STIX 2.1 export (experimental): https://www.m-cert.fr/admiral/admiral_bundle.json

**Please, do not use it to disclose private incidents, but contact your CSIRT organization, such as the Maritime Computer Emergency Response Team (M-CERT, https://www.m-cert.fr ).**

# Licensing and dissemination/reuse

This dataset is released under the Creative Commons CC-BY-NC license.
Copyright France Cyber Maritime


To use this content for research paper or other kind of publication (media, social networks),
please quote it:
- If you make a direct use of the CSV file, quote "Advanced Dataset of Maritime cyber Incidents ReleAsed for Litterature, ADMIRAL dataset, retrieved from https://gitlab.com/m-cert/admiral"
- If you use the ADMIRAL data from the web content (https://www.m-cert.fr/admiral), quote "Advanced Dataset of Maritime cyber Incidents ReleAsed for Litterature, ADMIRAL dataset, retrieved from https://www.m-cert/admiral"
Also quote the source when you derive data from it, such as graphs.

Finally, if you use the dataset in a scientific paper, you can quote the thesis of the original author:
```
@PHDTHESIS{Jacq2021,
url = "http://www.theses.fr/2021IMTA0228",
title = "Détection, analyse contextuelle et visualisation de cyber-attaques en temps réel : élaboration de la Cyber Situational Awareness du monde maritime",
author = "Jacq, Olivier",
year = "2021",
note = "Thèse de doctorat dirigée par Kermarrec, Yvon Informatique Ecole nationale supérieure Mines-Télécom Atlantique Bretagne Pays de la Loire 2021",
note = "2021IMTA0228",
url = "http://www.theses.fr/2021IMTA0228/document",
}
```

Or one of the articles of the original author of the dataset:
```
@inproceedings{jacq2021cyber,
  title={The Cyber-MAR project: First results and perspectives on the use of hybrid cyber ranges for port cyber risk assessment},
  author={Jacq, Olivier and Salazar, Pablo Gim{\'e}nez and Parasuraman, Kamban and Kuusij{\"a}rvi, Jarkko and Gkaniatsou, Andriana and Latsa, Evangelia and Amditis, Angelos},
  booktitle={2021 IEEE International Conference on Cyber Security and Resilience (CSR)},
  pages={409--414},
  year={2021},
  organization={IEEE}
}
