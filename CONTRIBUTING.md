# ADMIRAL
ADMIRAL is the "five-star" maritime cybersecurity incidents dataset!

# About the content
- The idea of this dataset is to help sharing a common knowledge of worldwide maritime cyber incidents.
- Sharing is caring, and it's still better in the long term to have a common repository than separate and uncoordinated initiatives. 
  It litteraly took years to build ADMIRAL up!
- This knowledge can then be used:
  - to increase the awareness of the maritime sector on incidents
  - hopefully, to contribute to the reduction of new incidents
  - to help researchers, students, specialized journalists and maritime or cybersecurity professionals work on the subject, such
    as identifying trends, working on threat assessment or risk assessment with realistic data.
- The data is provided AS IS, meaning its quality and comprehensiveness depends
  on the source of the information. However, we do our best to keep it accurate and up to date.
- Our goal is not to point at the victims. We all can be victims. They are quoted, in the references or in the file, to make the identification easier and avoid duplicates.
- Disclosed incidents are just a probably small part of what really exists, so be careful when derivating hypothesis, conclusions, research subjects or diagrams from this source.
- The data is shared as a CSV file, to help you build information from it with the format you wish.

# Citing the content
To use this content for research paper or other kind of publication (media, social networks),
please quote it:
- If you make a direct use of the CSV file, quote "Advanced Dataset of Maritime cyber Incidents ReleAsed for Litterature, ADMIRAL dataset, retrieved from https://gitlab.com/m-cert/admiral"
- If you use the ADMIRAL data from the web content (https://www.m-cert.fr/admiral), quote "Advanced Dataset of Maritime cyber Incidents ReleAsed for Litterature, ADMIRAL dataset, retrieved from https://www.m-cert/admiral"
Also quote the source when you derive data from it, such as graphs.

Finally, if you use the dataset in a scientific paper, you can quote the thesis of the original author:
```
@PHDTHESIS{Jacq2021,
url = "http://www.theses.fr/2021IMTA0228",
title = "Détection, analyse contextuelle et visualisation de cyber-attaques en temps réel : élaboration de la Cyber Situational Awareness du monde maritime",
author = "Jacq, Olivier",
year = "2021",
note = "Thèse de doctorat dirigée par Kermarrec, Yvon Informatique Ecole nationale supérieure Mines-Télécom Atlantique Bretagne Pays de la Loire 2021",
note = "2021IMTA0228",
url = "http://www.theses.fr/2021IMTA0228/document",
}
```


Or one of the articles of the original author of the dataset:
```
@inproceedings{jacq2021cyber,
  title={The Cyber-MAR project: First results and perspectives on the use of hybrid cyber ranges for port cyber risk assessment},
  author={Jacq, Olivier and Salazar, Pablo Gim{\'e}nez and Parasuraman, Kamban and Kuusij{\"a}rvi, Jarkko and Gkaniatsou, Andriana and Latsa, Evangelia and Amditis, Angelos},
  booktitle={2021 IEEE International Conference on Cyber Security and Resilience (CSR)},
  pages={409--414},
  year={2021},
  organization={IEEE}
}
```

# Contributing to the repository

By contributing to the ADMIRAL dataset, you accept and agree to the the LICENSE and the following guidelines.
You reserve all right, title, and interest in and to Your Contributions.
 
You are welcome to add issues and pull requests to improve the dataset.
Your Pull Request will be reviewed by one of our devs/volunteers and you will be asked to reformat it if needed. We don't bite and we will try to be as flexible as possible, so don't get intimidated and feel free to ask for support!

When suggesting for the addition of new or past incidents:
- please make sure they are not already listed ;-)
- please respect the case of the terms to be chosen to avoid duplicated,
- please avoid creating new categories if not necessary;
- please use the CSV format described below:

|YYYY_indexnumber|Year|Month|Day|Country|Target|Type of incident|Incident detail|Origin|Main impact|Reference|V|CRTA|D| where :
- "YYYY_indexnumber" in the year in 4 digits, where indexnumber is the current MAX value for the index of the year in question and
  add 1.
- "Year" is the Year in 4 digits when the incident occured (if known), or the date of the first disclosure report published.
- "Month" is the Month in 2 digits when the incident occured (if known), or the date of the first disclosure report published.
- "Day" is the Day in 2 digits when the incident occured (if known), or the the first disclosure report published.
  If unsure or unknown, insert "XX".
- "Country" is the country is the ISO 3166-1 alpha-2 2 letter country code where the incident occured. In case of a global event,
  add the ISO code of the country of the headquarters company.
  If unsure or unknown, insert "XX"
- "Target" is the maritime domain target to be chosen between: Port, Logistics, Shipowner, Industry, Defence, Ship, Offshore, Shipyard,Organisation, Administration, Fishing, MRE, Manufacturer, Transport, IT Services, Classification Company, Fluvial, Insurer, Education, Undersea Cable.
- "Type of Incident" is the type of incident, to be chosen between: Data leak, Denial of Service, GPS/AIS jamming/spoofing, Human
  error, Intrusion, Malfunction, Phishing, Physical hijack, Remote Access, Scam, Spearphishing, Undisclosed, Waterholing Attack,
  Website compromission.
- "Incident detail" gives a summary of the incident.
- "Origin" gives and idea about the source threat of the incident (Political, Cybercrime, Hacktivism, Espionage, Human error, Script Kiddie, Internal, Crime, Design, Scam). As the precise imputation is sometimes hard to determine, the use of Undisclosed should prevail in this case.
- "Reference": if possible, add the initial first reference reporting the incident. Other references are possible if they add updates, technical details, etc.
- "V" is the name of the reported victim, only if it was made publicly available.
- "CRTA" is the Claimed/Reported Threat Actor: for the moment, it can be the ransomware or DDoS operator, APT, etc.
- "D" is a base64-gzipped text with more detail about the event, when available.
Please stick as close as possible to the guidelines. That way we ensure the quality of the data and ease the requests mergings.

## License
See [LICENSE](LICENSE).

## Merge Request title
Try to be as descriptive as you can in your Merge Request title, such as:
* [YYYY_indexnumber] Incident
And just add the current CSV file with your update(s)
